require 'rails_helper'

RSpec.describe StatusController, type: :routing do
  describe 'routing' do
    it 'routes to #index' do
      expect(get: '/status').to route_to 'status#index'
    end
  end

  describe 'route helpers' do
    it { expect(status_path).to eq '/status' }
  end
end
