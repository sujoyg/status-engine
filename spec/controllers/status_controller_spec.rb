require 'rails_helper'

describe StatusController, :type => :controller do
  describe 'GET index' do
    it 'is successful' do
      get :index

      expect(response).to have_http_status :success
    end

    it 'renders the template' do
      get :index

      expect(response).to render_template 'status/index'
    end
  end
end

