require 'rails_helper'

describe 'status/index', :type => :view do
  it 'renders information about latest git commit' do
    render

    `git log -n1`.lines.each do |line|
      expect(rendered).to have_content line
    end
  end
end
