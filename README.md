## Prerequisites
Status engine requires Rails 4.1.x.

## Installation

* Add the following to the `Gemfile`:  
  `gem 'status_engine'`
* Run `bundle install`

That's it!  

You should see information about the latest deployed git commit at /status.