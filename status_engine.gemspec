$:.push File.expand_path('../lib', __FILE__)

require 'status_engine/version'

Gem::Specification.new do |s|
  s.name        = 'status_engine'
  s.version     = VERSION
  s.authors     = ['Sujoy Gupta']
  s.email       = ['sujoyg@gmail.com']
  s.homepage    = 'https://bitbucket.org/sujoyg/status_engine'
  s.summary     = 'Rails engine for displaying app status.'
  s.description = 'This engine enables displaying the app status at /status.'
  s.license     = 'MIT'
  s.files = Dir['{app,config,db,lib}/**/*'] + ['MIT-LICENSE', 'Rakefile']

  s.add_dependency 'rails', '~> 4.1'

  s.add_development_dependency 'capybara', '2.4'
  s.add_development_dependency 'rspec-rails', '~> 3.1'
  s.add_development_dependency 'specstar-controllers', '~> 0.2'
  s.add_development_dependency 'sqlite3'
end
